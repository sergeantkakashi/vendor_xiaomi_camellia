# Vendor tree for Redmi Note 10 5G/Redmi Note 10T/Redmi Note 11 SE/POCO M3 Pro
The Redmi Note 10 5G/Redmi Note 10T/Redmi Note 11 SE/POCO M3 Pro (codenamed "camellia" (china)/"camellian" (global)) is mid-end, mid-range device from Xiaomi. It was release in May 2021

| **Basic**               | **Spec sheet**                                                                           |
|-------------------------|------------------------------------------------------------------------------------------|
| CPU                     | Octa-core (2x2.2 GHz Cortex-A76 & 6x2.0 GHz Cortex-A55)                                  |
| Chipset                 | MT6833                                                                                   |
| GPU                     | Mali-G57 MC2                                                                             |
| Memory                  | 4/6 GB (Global) 4/6/8 GB (China)                                                                           |
| Shipped Android version | 11.0                                                                                     |
| Storage                 | 64/128 GB (Global) 128/256 GB (China)                                                                        |
| Battery                 | Li-Po 5000 mAh, non-removable                                                            |
| Display                 | 1080 x 2400 pixels, 20:9 ratio (~405 ppi density)                                        |
| Camera (Back)(Main)     | 48 MP, f/1.8, 26mm (wide), 1/2.0", 0.8µm, PDAF 2 MP, f/2.4, (macro) 2 MP, f/2.4, (depth) |
| Camera (Front)          | 8 MP, f/2.0, (wide)                                                                      |

# Images
**Redmi Note 105G/Redmi Note 10T**<br>
![xiaomi-redmi-note10-5g-0](https://user-images.githubusercontent.com/98401886/210569626-1789d2d9-e853-4bf4-a573-d255947866a8.jpg)
<br>**Redmi Note 11 SE**<br>
![xiaomi-redmi-note-11se-1](https://user-images.githubusercontent.com/98401886/210569665-bf16cd25-d74b-45ea-b83a-660d04226105.jpg)
<br>**POCO M3 Pro**<br>
![xiaomi-poco-m3-pro-5g-3](https://user-images.githubusercontent.com/98401886/210569689-86d59ea1-1334-4c4a-b04e-148c3c796409.jpg)
